package com.kepler.assignment_networkcall.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONObject;

public class RequestCall {

    public static final String BASE = "https://jsonplaceholder.typicode.com/";
    public static final String URL_APPENDER_ALBUMS = "albums";
    public static final String URL_APPENDER_PHOTOES = "photos?albumId=";
    private static JsonObjectRequest jsonObjectRequest;
    private static StringRequest stringRequest;

    public static void jsonRequest(Context mContext, String urlAppender, final OnResponseReceived onResponseReceived) {
        jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, BASE + urlAppender, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        onResponseReceived.onResponse(new Gson().fromJson(response.toString(), onResponseReceived.getType()));
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        onResponseReceived.onError(error.getMessage());

                    }
                });

// Access the RequestQueue through your singleton class.
        ApiClient.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    public static void stringRequest(Context mContext, String urlAppender, final OnResponseReceived onResponseReceived) {
        stringRequest = new StringRequest
                (Request.Method.GET, BASE + urlAppender, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        onResponseReceived.onResponse(new Gson().fromJson(response.replaceAll("\n",""), onResponseReceived.getType()));
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        onResponseReceived.onError(error.getMessage());

                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Priority getPriority() {
                return Priority.IMMEDIATE;
            }
        };


// Access the RequestQueue through your singleton class.
        ApiClient.getInstance(mContext).addToRequestQueue(stringRequest);
    }


}
