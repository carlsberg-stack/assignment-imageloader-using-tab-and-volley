package com.kepler.assignment_networkcall.api;

import com.google.gson.reflect.TypeToken;
import com.kepler.assignment_networkcall.Pojo.Album;

import java.lang.reflect.Type;
import java.util.List;

public interface OnResponseReceived<T> {

    Class getType();

    void onResponse(T response);

    void onError(String errorMessage);
}
