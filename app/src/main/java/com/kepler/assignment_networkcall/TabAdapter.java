package com.kepler.assignment_networkcall;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.kepler.assignment_networkcall.Pojo.Album;

import java.util.ArrayList;
import java.util.List;

public class TabAdapter extends FragmentStatePagerAdapter
{
    private final List<Album> albums;
    public TabAdapter(FragmentManager fm, List<Album> albums) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.albums=albums;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = new PhotoFragement();
        Bundle args = new Bundle();
        // Our object is just an integer :-P
        args.putInt(PhotoFragement.ARG_OBJECT, albums.get(i).getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return 100;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return albums.get(position).getTitle();
    }

}

// Instances of this class are fragments representing a single
