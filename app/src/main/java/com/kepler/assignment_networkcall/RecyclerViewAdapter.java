package com.kepler.assignment_networkcall;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.kepler.assignment_networkcall.Pojo.Photo;
import com.kepler.assignment_networkcall.api.ApiClient;

import java.util.List;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.LayoutViewHolder> {

    private final List<Photo> dataSet;
    private final ImageLoader imageLoader;

    public RecyclerViewAdapter(List<Photo> asList, ImageLoader imageLoader) {
        dataSet = asList;
        this.imageLoader = imageLoader;
        // Instantiate the RequestQueue.
//        mImageLoader = ApiClient.getInstance()
//                .getImageLoader();
//        //Image URL - This can point to any image file supported by Android
//        mImageLoader.get(url, ImageLoader.getImageListener(mNetworkImageView,
//                R.mipmap.truiton_short, android.R.drawable
//                        .ic_dialog_alert));
    }

    @Override
    public LayoutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
        return new LayoutViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LayoutViewHolder holder, int position) {
        Photo object = dataSet.get(position);
        holder.title.setText(object.getTitle());
        imageLoader.get(object.getUrl(), ImageLoader.getImageListener(holder.image_view,
                R.mipmap.ic_launcher_round, android.R.drawable
                        .ic_dialog_alert));
        holder.image_view.setImageUrl(object.getThumbnailUrl(),imageLoader);
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public class LayoutViewHolder extends RecyclerView.ViewHolder {


        public final NetworkImageView image_view;
        public final TextView title;

        public LayoutViewHolder(View itemView) {
            super(itemView);
            image_view = itemView.findViewById(R.id.image_view);
            title = itemView.findViewById(R.id.title);
        }

    }

}