package com.kepler.assignment_networkcall;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kepler.assignment_networkcall.Pojo.Photo;
import com.kepler.assignment_networkcall.api.ApiClient;
import com.kepler.assignment_networkcall.api.OnResponseReceived;
import com.kepler.assignment_networkcall.api.RequestCall;

import java.util.Arrays;

public class PhotoFragement extends Fragment {
    public static final String ARG_OBJECT = "arg_object";
    private RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView recyclerView;
    private int albumId;
    private ProgressBar progressbar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        albumId = getArguments().getInt(ARG_OBJECT);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        progressbar = view.findViewById(R.id.progressbar);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RequestCall.stringRequest(getContext(), RequestCall.URL_APPENDER_PHOTOES + albumId, new OnResponseReceived<Photo[]>() {

            @Override
            public Class getType() {
                return Photo[].class;
            }

            @Override
            public void onResponse(Photo[] response) {
                progressbar.setVisibility(View.GONE);
                recyclerViewAdapter = new RecyclerViewAdapter(Arrays.asList(response), ApiClient.getInstance(getActivity()).getImageLoader());
                GridLayoutManager manager = new GridLayoutManager(getActivity(), 3, RecyclerView.VERTICAL, false);
                recyclerView.setLayoutManager(manager);
                recyclerView.setAdapter(recyclerViewAdapter);
            }

            @Override
            public void onError(String errorMessage) {
                progressbar.setVisibility(View.GONE);
            }
        });
    }
}
