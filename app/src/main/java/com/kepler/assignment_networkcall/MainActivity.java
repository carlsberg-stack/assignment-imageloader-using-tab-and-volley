package com.kepler.assignment_networkcall;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.kepler.assignment_networkcall.Pojo.Album;
import com.kepler.assignment_networkcall.api.OnResponseReceived;
import com.kepler.assignment_networkcall.api.RequestCall;
import com.kepler.assignment_networkcall.source.BaseActivity;
import com.kepler.assignment_networkcall.source.CustomViewPager;

import java.util.Arrays;


public class MainActivity extends BaseActivity {

    ProgressBar progressbar;
    CustomViewPager viewPager;
    TabLayout tabLayout;
    private TabAdapter tabAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        RequestCall.stringRequest(getApplicationContext(), RequestCall.URL_APPENDER_ALBUMS, new OnResponseReceived<Album[]>() {

            @Override
            public Class getType() {
                return Album[].class;
            }

            @Override
            public void onResponse(Album[] response) {
                progressbar.setVisibility(View.GONE);
                int i = 0;
                while (i < response.length) {
                    tabLayout.addTab(tabLayout.newTab());
                    i++;
                }
                tabAdapter = new TabAdapter(getSupportFragmentManager(), Arrays.asList(response));
                viewPager.setAdapter(tabAdapter);
                tabLayout.setupWithViewPager(viewPager);
            }

            @Override
            public void onError(String errorMessage) {
                progressbar.setVisibility(View.GONE);
                toast(errorMessage);
            }
        });
    }

    private void init() {
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.pager);
        progressbar = findViewById(R.id.progressbar);
    }
}
